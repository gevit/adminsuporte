#!/bin/bash
#Admin Suporte
#Versão 1.2
#Modif 21/01/2019
clear

echo "---------------------------------------------"
echo " _     _    . .   ___   . .                  "
echo "|_|   | \   |V|    |    |\|                  "
echo "| |   |_/   | |   _|_   | | Suporte       1.2"
echo "---------------------------------------------"

#-------------------PROCESSOS-------------------

	echo -ne "Mysql............."
	if ! hash mysql 2>/dev/null; then
		echo -e "Parado"
		exit=1
	else
		echo -e "OK!"
	fi
		echo -ne "Postgres............."
	if ! hash postgresql 2>/dev/null; then
		echo -e "Parado"
		exit=1
	else
		echo -e "OK!"
	fi
			echo -ne "Asterisk............."
	if ! hash asterisk 2>/dev/null; then
		echo -e "Parado"
		exit=1
	else
		echo -e "OK!"
	fi
			echo -ne "Khomp-Services............."
	if ! hash khomp-services 2>/dev/null; then
		echo -e "Parado"
		exit=1
	else
		echo -e "OK!"
	fi
			echo -ne "Awk............."
	if ! hash awk 2>/dev/null; then
		echo -e "Parado"
		exit=1
	else
		echo -e "OK!"
	fi
	echo -ne "Squid............."
	if ! hash squid 2>/dev/null; then
		echo -e "Parado"
		exit=1
	else
		echo -e "OK!"
	fi
	echo -ne "Courier-authlib............."
	if ! hash courier-authlib 2>/dev/null; then
		echo -e "Parado"
		exit=1
	else
		echo -e "OK!"
	fi
	echo -ne "Courier-pop3............."
	if ! hash courier-pop3 2>/dev/null; then
		echo -e "Parado"
		exit=1
	else
		echo -e "OK!"
	fi
	echo -ne "Courier-imap............."
	if ! hash courier-imap 2>/dev/null; then
		echo -e "Parado"
		exit=1
	else
		echo -e "OK!"
	fi
	echo -ne "Qmail............."
	if ! hash courier-qmail 2>/dev/null; then
		echo -e "Parado"
		exit=1
	else
		echo -e "OK!"
	fi
	echo -ne "Mav5............."
	if ! hash mav5 2>/dev/null; then
		echo -e "Parado"
		exit=1
	else
		echo -e "OK!"
	fi

#--------------MENU PRINCIPAL--------------

Menu(){
echo "------------------------------------------"
   echo "[ 1 ] Planetfone"
   echo "[ 2 ] Webgateway"
   echo "[ 3 ] Discadora"
   echo "[ 4 ] Linux"
   echo "[ 99 ] Sair"
   echo -n "Analisar qual sistema ? "
   read opcao
	 clear

case $opcao in
		1) toolspf;; 2) toolswg;; 3) toolsd;;
		4) toolslinux;;	99) exit;;
esac
}

#--------------Menu Planetfone--------------

toolspf(){
echo "------------------------------------------"
   echo "[ 1 ] Peers Ativos"
   echo "[ 2 ] Ligações"
   echo "[ 3 ] Status Troncos"
   echo "[ 4 ] Status Canais Khomp"
	 echo "[ 5 ] Status Khomp"
	 echo "[ 6 ] Restartar Asterisk"
	 echo "[ 7 ] Apagar logs Asterisk"
   echo "[ 99 ] Voltar"
   echo -n "Qual a opcao desejada ? "
	 read op_planetfone
   clear

 case $op_planetfone in
	 1) cons_peer ;; 2) cons_lig ;; 3) cons_troncos;;
	 4) cons_chan_khomp;; 5) cons_status_khomp ;; 6) restart_asterisk;;
	 7) apaga_log_asterisk ;; 99) Menu ;;
esac
}

#------------------PLANETFONE TOOLS-----------------

cons_peer(){
	echo "Peers ativos:"
	asterisk -rx 'sip show peers' | grep -v "Uns\|UNREA"
	toolspf
}

cons_lig(){
	echo "Ligações ativas:"
	asterisk -rx 'core show channels'
	toolspf
}

cons_troncos(){
	echo "Opção indisponivel no momento."
	toolspf
}

cons_chan_khomp(){
	echo "Canais ativos Khomp:"
	asterisk -rx 'khomp channels show'
	toolspf
}

cons_status_khomp(){
	echo "Status Khomp:"
	asterisk -rx 'khomp summary'
	toolspf
}

restart_asterisk(){
	echo "Parando Asterisk.."
        wait 2
	service asterisk stop
        echo "--------------"
	echo "Asterisk parado com sucesso"
        wait 2
	echo "Restartando Khomp.."
        echo "--------------"
  /etc/init.d/khomp-services restart
	echo "Restart Khomp com sucesso"
        wait 2
	echo "Asterisk iniciando.."
	service asterisk start
        echo "--------------"
	echo "Asterisk iniciado com sucesso"
	toolspf
}

apaga_log_asterisk() {
	echo "Rotacionando logs asterisk.."
	asterisk -rx 'logger rotate'
	asterisk -rx 'logger reload'
  echo "Logs rotacionados."
	cd /var/log/asterisk
	echo "Iniciando exclusão dos logs.."
	rm -rfv full.* queue_log.* messages.* event_log.* full_*
	echo "Logs apagados."
	cd
	toolspf
}

#--------------Menu Gateway--------------

toolswg(){
echo "------------------------------------------"
   echo "[ 1 ] Monitorar Proxy"
   echo "[ 2 ] Monitorar Emails"
	 echo "[ 3 ] Apagar cache Squid"
	 echo "[ 4 ] Apagar logs Squid"
   echo "[ 99 ] Voltar"
   echo -n "Qual a opcao desejada ? "
	 read op_webgateway
   clear

 case $op_webgateway in
	 1) mon_proxy ;; 2) mon_email ;;
	 3) del_cache ;; 4) del_log ;;
	 99) Menu ;;
esac
}

#--------------GATEWAY TOOLS--------------
        del_cache(){
        echo "Entrando no Spool Squid"
        cd /var/spool/squid
				echo "Parando Squid"
        service squid stop
				echo "Squid parado com sucesso."
        echo "Excluindo cache.. Aguarde"
        rm -rf 0*
				echo "Cache excluido com sucesso."
				echo "Iniciando Squid"
        service squid start
				echo "Squid iniciado com sucesso."
        echo "Cache excluido."
        toolswg
               }

        del_log(){
            echo "Entrando no Log Squid"
            cd /var/log/squid
            echo "Excluindo log.. Aguarde"
            rm -rf access.log.* cache.log.*
            echo "Logs excluidos."
            toolswg
             }

				mon_proxy(){
					tail -f /var/log/squid/access.log
				}

				mon_email(){
					tail -f /var/log/maillog.log
				}

#--------------Menu Linux--------------

				toolslinux(){
					echo "------------------------------------------"
				   echo "[ 1 ] Adicionar Usuario"
				   echo "[ 2 ] Atualizar a lista de pacotes"
				   echo "[ 3 ] Backup"
				   echo "[ 4 ] Instalar programa"
					 echo "[ 5 ] Atualizar Linux"
					 echo "[ 6 ] Informações da máquina"
					 echo "[ 7 ] Testar HD"
					 echo "[ 8 ] Particionamento"
					 echo "[ 9 ] Uptime e carga"
					 echo "[ 10 ] Validar links ativos"
				 	 echo "[ 11 ] Apagar Logs Linux"
				   echo "[ 99 ] Voltar"
				   echo -n "Qual a opcao desejada ? "
				   read op_linux
					 clear

				   case $op_linux in
				      1) add_user ;; 2) att_pkt ;; 3) backup ;;
				      4) install_prog ;; 5) att_linux ;; 6) hardware_info ;;
							7) test_hd ;; 8) partic ;; 9) uptime_load ;; 10) validalink ;;
							11) apaga_log_linux ;; 99) Menu ;;
				      *) "Opcao desconhecida.";;
				   esac
				}

#----------------LINUX TOOLS-------------------
add_user() {
echo "Digite o nome do usuário"
read usuario
useradd $usuario
echo "Informe a senha para o novo usuário"
passwd $usuario
   toolslinux
}

att_pkt() {

echo "Atualizando a lista de pacotes"
apt-get update
clear
toolslinux
}

att_linux(){
	apt-get dist-upgrade
	clear
	toolslinux
}

#---------------------------------------------------------------

hardware_info(){
clear
./root/hardware.sh
toolslinux
}

#---------------------------------------------------------------

backup() {
echo "Especifique o local a ser backupeado"
read local

echo "+++++++++++++++++++++++++++++++++++++++"
echo

echo "Especifique nome do backup =)"
read nome

echo

echo
echo "+++++++++++++++++++++++++++++++++++++++"

echo "Especifique o destino do backup =)"
read destino

sudo tar cvf $destino/$nome.tar $local
cd $destino
ls $nome
toolslinux
}

install_prog() {
echo "Digite o nome do programa que deseja instalar"
read programa
yum install $programa
toolslinux
}

uptime_load(){
	clear
  echo "Tempo ativo e carga da máquina:"
	uptime
	toolslinux
}

partic(){
	clear
	df -h
	toolslinux
	}

	test_hd(){
					clear
					echo "Tentando criar arquivo 'teste'..."
					touch teste
					echo "Arquivo criado. HD funcional."
					echo "Rodando hdparm, aguarde..."
					hdparm -t /dev/sda
					echo "Fim."
					toolslinux
					}

					         validalink(){
					            clear
					            echo "Links ativos"
					            mii-tool | grep "link ok"
					            toolslinux
										}
apaga_log_linux(){
	echo "Entrando no /var/log.."
	cd /var/log
	echo "Iniciando exclusão dos logs.."
	rm -rfv boot.log.* cron.* fail2ban.log.* kernel.log.* maillog.* messages.* pf.processaqueuelog.log.* rpmpkgs.* secure.* spooler.* stund.log.* wtmp.* xferlog.* yum.log.* zebedee.log.*
	echo "Logs apagados."
	cd
	toolslinux

}

#--------------Menu Discadora--------------

toolsd(){
	echo "------------------------------------------"
   echo "Em construção"
   echo "[ 99 ] Voltar"
   echo -n "Qual a opcao desejada ? "
   read op_discad
	 clear

   case $op_discad in
      99) Menu ;;
   esac
}
            Menu
